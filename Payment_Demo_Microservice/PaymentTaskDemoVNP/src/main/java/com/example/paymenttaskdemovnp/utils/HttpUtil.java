package com.example.paymenttaskdemovnp.utils;

import com.example.paymenttaskdemovnp.config.HttpConfig;
import com.example.paymenttaskdemovnp.httpclient.HttpClientPool;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.nio.client.HttpAsyncClient;

import java.util.concurrent.Future;

@Slf4j
public class HttpUtil {
    private static HttpAsyncClient httpClient = HttpClientPool.getInstance().getHttpClient();

    /**
     * Post HttpClient with URI, body, get HttpResponse in callback
     * @param uri
     * @param body
     * @param callback
     */
    public static Future<HttpResponse> post(String uri, String body, FutureCallback<HttpResponse> callback) {
        HttpPost request = new HttpPost(uri);
        request.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
        log.info("Execute post with url: {}, body : {}", uri, body);
        log.info("Request: {}", request);
        Future<HttpResponse> future = httpClient.execute(request, callback);
        return future;
    }

    public static void initHttpConnection() {
        CloseableHttpAsyncClient httpClient = HttpClientPool.getInstance().getHttpClient();
        if (httpClient == null) {
            log.info("Fail to create Http connection pool");
            return;
        }
        httpClient.start();
    }
}
