package com.example.paymenttaskdemovnp.response;

public enum ResponseCode implements IResponseCode {

    SUCCESS("00", "Success"),
    INVALID_REQUEST("01", "Invalid request"),
    NOT_IN_FORMAT("02", "Data input is not in format."),
    NOT_FOUND("04", "Not found."),
    DATE_RANGE_INVALID("15", "Invalid search interval."),
    INVALID_TIME_RANGE("98", "Time range for searching is invalid!"),
    MAINTENANCE("96", "System is maintain."),
    EXCEPTION("99", "Internal System Error...!"),
    FORBIDDEN("403","Access is denied"),
    LIST_EXPORT_EMPTY("404","Không có dữ liệu để xuất báo cáo");

    ResponseCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}