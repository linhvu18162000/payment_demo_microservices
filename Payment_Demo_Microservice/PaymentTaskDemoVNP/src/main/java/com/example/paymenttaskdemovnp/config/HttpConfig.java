package com.example.paymenttaskdemovnp.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Data
@PropertySource("classpath:application.properties")
public class HttpConfig {

    @Value("${http.client.url}")
    private String url;

    @Value("${http.client.pool.connection-timeout}")
    private String connectionTimeout;

    @Value("${http.client.pool.max-total}")
    private String maxTotal;

    @Value("${http.client.pool.max-per-route}")
    private String maxPerRoute;

    @Value("${http.client.pool.socket-timeout}")
    private String socketTimeout;
}
