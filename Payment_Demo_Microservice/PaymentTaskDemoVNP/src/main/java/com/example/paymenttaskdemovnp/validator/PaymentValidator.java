package com.example.paymenttaskdemovnp.validator;

import com.example.paymenttaskdemovnp.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp.response.BaseResponse;
import com.example.paymenttaskdemovnp.response.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;

@Slf4j
public class PaymentValidator {

    /**
     * validate body for payment method
     * @param paymentDataDTO
     * @return BaseResponse
     */
    public static BaseResponse validatorBody(PaymentDataDTO paymentDataDTO) {
        if (Strings.isBlank(paymentDataDTO.getTokenKey())) {
            log.info("token Key is cannot be null");
            return BaseResponse.of(ResponseCode.INVALID_REQUEST);
        }

        return null;
    }
}
