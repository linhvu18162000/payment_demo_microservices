package com.example.paymenttaskdemovnp.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GsonUtils {
    private static Gson Gson = new GsonBuilder().serializeNulls().create();

    /**
     * @param obj
     * @return
     */
    public static String bean2Json(Object obj) {
        return Gson.toJson(obj);
    }

    /**
     * convert String to Object
     * @param json        Object
     * @param classObject Class
     * @return object of classObject
     * return null if parsing has Exception
     */
    public static <T> T parseStringToObject(String json, Class<T> classObject) {
        try {
            return Gson.fromJson(json, classObject);
        } catch (Exception ex) {
            log.error("Convert json to bean have ex: ", ex);
            return null;
        }
    }

}