package com.example.paymenttaskdemovnp.service;

import com.example.paymenttaskdemovnp.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp.response.BaseResponse;

/**
 * author: ngoclt1
 */
public interface IPaymentService {
    public BaseResponse insertNewPayment(PaymentDataDTO paymentDataDTO);
}
