package com.example.paymenttaskdemovnp.response;

public interface IResponseCode {

    String getCode();

    String getMessage();
}
