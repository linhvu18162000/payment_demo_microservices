package com.example.paymenttaskdemovnp.httpclient.response;

import com.google.common.io.CharStreams;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Slf4j
public class PaymentResponse implements FutureCallback<HttpResponse> {
    @Override
    public void completed(HttpResponse httpResponse) {
        log.info("HttpResponse from Payment: [{}]", httpResponse);
        String content = null;
        try {
            content = CharStreams.toString(new InputStreamReader(httpResponse.getEntity().getContent(),
                    StandardCharsets.UTF_8));
            log.info("Response content from Payment:  [{}]", content);

        } catch (IOException e) {
            log.error("Handle payment response have exception: ", e);
        }
    }

    @Override
    public void failed(Exception e) {

    }

    @Override
    public void cancelled() {

    }
}
