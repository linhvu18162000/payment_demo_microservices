package com.example.paymenttaskdemovnp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDataDTO {
    @NotNull(message = "tokenKey is not null")
    private String tokenKey;

    private String apiID;

    private String mobile;

    private String bankCode;

    private String accountNo;

    private String payDate;

    private String addtionalData;

    private String debitAmount;

    private String respCode;

    private String respDesc;

    private String traceTransfer;

    private String messageType;

    private Item[] item;

    private String checkSum;

    private String orderCode;

    private String userName;

    private String realAmount;

    private String promotionCode;

    private String addValue;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Item {
        private String qrInfor;

        private String quantity;

        private String note;
    }
}
