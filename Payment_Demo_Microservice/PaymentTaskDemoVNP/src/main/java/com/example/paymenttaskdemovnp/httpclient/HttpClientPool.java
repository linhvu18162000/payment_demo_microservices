package com.example.paymenttaskdemovnp.httpclient;

import com.example.paymenttaskdemovnp.constant.Constant;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;

@Slf4j
public class HttpClientPool {

    public static HttpClientPool getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        protected static final HttpClientPool INSTANCE = new HttpClientPool();
    }

    @Getter
    private final CloseableHttpAsyncClient httpClient;

    private HttpClientPool() {
        IOReactorConfig ioReactorConfig = IOReactorConfig.custom()
                .setIoThreadCount(Runtime.getRuntime().availableProcessors())
                .setConnectTimeout(Constant.HTTP_CONNECTION_TIMEOUT)
                .build();
        ConnectingIOReactor ioReactor;
        try {
            ioReactor = new DefaultConnectingIOReactor(ioReactorConfig);
        } catch (IOReactorException e) {
            log.info("Create http client pool have exception :", e);
            httpClient = null;
            return;
        }
        PoolingNHttpClientConnectionManager cm = new PoolingNHttpClientConnectionManager(ioReactor);
        cm.setMaxTotal(Constant.HTTP_MAX_TOTAL);
        cm.setDefaultMaxPerRoute(Constant.HTTP_DEFAULT_MAX_PER_ROUTE);
        RequestConfig requestConfig = RequestConfig
                .custom()
                .setConnectTimeout(Constant.HTTP_CONNECTION_TIMEOUT)
                .setSocketTimeout(Constant.HTTP_SOCKET_TIMEOUT)
                .build();
        httpClient = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }
}
