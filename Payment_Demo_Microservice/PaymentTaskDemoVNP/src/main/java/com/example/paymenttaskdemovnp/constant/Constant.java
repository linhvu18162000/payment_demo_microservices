package com.example.paymenttaskdemovnp.constant;

public class Constant {
    /**
     * STATUS HTTP CODE
     */
    public static final int STATUS_OK = 200;

    /**
     * HTTP CONSTANTS
     */
    public static final String URL_SERVICE_B = "http://localhost:9001/payment/create";
    public static final int HTTP_CONNECTION_TIMEOUT = 3000;
    public static final int HTTP_MAX_TOTAL = 5;
    public static final int HTTP_DEFAULT_MAX_PER_ROUTE = 4;
    public static final int HTTP_SOCKET_TIMEOUT = 3000;
}
