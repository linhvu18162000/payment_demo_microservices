package com.example.paymenttaskdemovnp.service.impl;

import com.example.paymenttaskdemovnp.constant.Constant;
import com.example.paymenttaskdemovnp.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp.httpclient.response.PaymentResponse;
import com.example.paymenttaskdemovnp.response.BaseResponse;
import com.example.paymenttaskdemovnp.response.ResponseCode;
import com.example.paymenttaskdemovnp.service.IPaymentService;
import com.example.paymenttaskdemovnp.utils.GsonUtils;
import com.example.paymenttaskdemovnp.utils.HttpUtil;
import com.example.paymenttaskdemovnp.validator.PaymentValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentServiceImpl implements IPaymentService {

    /**
     * Receive request body paymentDataDTO, verify body of paymentDataDTO and send paymentDataDTO to ServiceB
     * @param paymentDataDTO: PaymentDataDTO
     * @return: Response Entity
     */
    @Override
    public BaseResponse insertNewPayment(PaymentDataDTO paymentDataDTO) {
        log.info("Start create new Payment with Request: {}", GsonUtils.bean2Json(paymentDataDTO));

        if (paymentDataDTO == null) {
            log.info("Payment Data DTO cannot be null");
            return BaseResponse.of(ResponseCode.INVALID_REQUEST);
        }

        BaseResponse validator = PaymentValidator.validatorBody(paymentDataDTO);
        if (validator != null) {
            log.info("Invalid Request, Request has validated fail");
            return validator;
        }

        log.info("Request body data of Payment is valid !");

        // Call to Service B
        HttpUtil.initHttpConnection();

        log.info("Start call to service B with url: {}", Constant.URL_SERVICE_B);
        Future<HttpResponse> future = HttpUtil.post(Constant.URL_SERVICE_B, GsonUtils.bean2Json(paymentDataDTO), new PaymentResponse());
        try {
            HttpResponse response = future.get();
            if (response.getStatusLine().getStatusCode() != Constant.STATUS_OK) {
                log.info("Response code from Service B is not success !");
                return BaseResponse.of(ResponseCode.EXCEPTION);
            }

            log.info("Create new Payment Successfully !");
            return BaseResponse.of(ResponseCode.SUCCESS);
        } catch (Exception e) {
            log.info("Exception when call to Service B: {}", e);
            return BaseResponse.of(ResponseCode.EXCEPTION);
        }
    }
}
