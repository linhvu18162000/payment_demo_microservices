package com.example.paymenttaskdemovnp.controller;

import com.example.paymenttaskdemovnp.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp.service.IPaymentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/payment")
@AllArgsConstructor
@Slf4j
public class PaymentController {
    private IPaymentService paymentService;

    /**
     * API Post request for payment demo
     * @param paymentDataDTO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> insertNewPayment(@Valid @RequestBody PaymentDataDTO paymentDataDTO) {
        return ResponseEntity.ok(paymentService.insertNewPayment(paymentDataDTO));
    }
}
