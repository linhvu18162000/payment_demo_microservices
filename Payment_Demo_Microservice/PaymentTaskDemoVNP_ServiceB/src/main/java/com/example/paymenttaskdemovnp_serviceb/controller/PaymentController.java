package com.example.paymenttaskdemovnp_serviceb.controller;

import com.example.paymenttaskdemovnp_serviceb.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp_serviceb.service.IPaymentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
@AllArgsConstructor
@Slf4j
public class PaymentController {

    private IPaymentService iPaymentService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> insertNewPayment(@RequestBody PaymentDataDTO paymentDataDTO) {
        return ResponseEntity.ok(iPaymentService.insertNewPayment(paymentDataDTO));
    }
}
