package com.example.paymenttaskdemovnp_serviceb.service;


import com.example.paymenttaskdemovnp_serviceb.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp_serviceb.response.BaseResponse;

/**
 * author: ngoclt1
 */
public interface IPaymentService {
    public BaseResponse insertNewPayment(PaymentDataDTO paymentDataDTO);
}
