package com.example.paymenttaskdemovnp_serviceb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDataDTO {
    private String tokenKey;

    private String apiID;

    private String mobile;

    private String bankCode;

    private String accountNo;

    private String payDate;

    private String addtionalData;

    private String debitAmount;

    private String respCode;

    private String respDesc;

    private String traceTransfer;

    private String messageType;

    private String checkSum;

    private String orderCode;

    private String userName;

    private String realAmount;

    private String promotionCode;

    private String addValue;
}
