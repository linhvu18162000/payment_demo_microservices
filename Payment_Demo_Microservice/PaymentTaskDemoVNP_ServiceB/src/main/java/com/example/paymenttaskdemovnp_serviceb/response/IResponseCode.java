package com.example.paymenttaskdemovnp_serviceb.response;

public interface IResponseCode {

    String getCode();

    String getMessage();
}
