package com.example.paymenttaskdemovnp_serviceb.service.impl;


import com.example.paymenttaskdemovnp_serviceb.dto.PaymentDataDTO;
import com.example.paymenttaskdemovnp_serviceb.response.BaseResponse;
import com.example.paymenttaskdemovnp_serviceb.response.ResponseCode;
import com.example.paymenttaskdemovnp_serviceb.service.IPaymentService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentServiceImpl implements IPaymentService {
    private final Gson gson;

    @Override
    public BaseResponse insertNewPayment(PaymentDataDTO paymentDataDTO) {
        log.info("Service B receive request: {}", gson.toJson(paymentDataDTO));

        log.info("Start insert to Database ");

        log.info("Insert to Database successfully !");
        return BaseResponse.of(ResponseCode.SUCCESS);
    }
}
