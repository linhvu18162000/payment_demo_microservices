package com.example.paymenttaskdemovnp_serviceb.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author ngoclt1
 * Date: 4/28/2021
 * Time: 2:02 PM
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BaseResponse implements Serializable {

    private String code;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;

    public static BaseResponse of(Object data) {
        return of(ResponseCode.SUCCESS, data);
    }

    public static BaseResponse of(IResponseCode responseCode) {
        return of(responseCode, null);
    }

    public static BaseResponse of(IResponseCode responseCode, Object data) {
        return of(responseCode.getCode(), responseCode.getMessage(), data);
    }

    public static BaseResponse of(String code, String message) {
        return of(code, message, null);
    }

    public static BaseResponse of(String code, String message, Object data) {
        return BaseResponse.builder().code(code).message(message).data(data).build();
    }

    public boolean equalByCode(IResponseCode responseCode) {
        if (responseCode.getCode().equals(code)) {
            return true;
        }
        return false;
    }

    public boolean isNotEqualByCode(IResponseCode responseCode) {
        if (responseCode.getCode().equals(code)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        boolean isCollection = data instanceof Collection;
        String dataToPrint = isCollection
                ? String.valueOf(((Collection) data).size()) : data.toString();
        return "BaseResponse{"
                + "code='" + code + '\''
                + ", message='" + message + '\''
                + ", data=" + dataToPrint
                + '}';
    }
}

